import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: window["BSMCH_CONFIG"].REACT_APP_OCEAN_ENDPOINT,
  request: operation => {
    const token = localStorage.getItem("adal.idtoken");
    operation.setContext({
      headers: {
        //"x-hasura-admin-secret": process.env.REACT_APP_HASURA_ADMIN_SECRET,
        authorization: token ? `Bearer ${token}` : ""
      }
    });
  }
});

export default client;
