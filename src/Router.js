import React from "react";
import { Switch, Route } from "react-router-dom";
import BulkInsertPage from "./fetchers/bulk-insert/BulkInsertPage";
import UsersAndGroupsPage from "./fetchers/manage-data/UsersAndGroupsPage";
import HomePage from "./components/HomePage";

const Router = () => (
  <Switch>
    <Route path="/" component={HomePage} exact></Route>
    <Route path="/group/:groupId/manage-data">
      <UsersAndGroupsPage />
    </Route>
    <Route path="/group/:groupId/bulk-insert">
      <BulkInsertPage />
    </Route>
  </Switch>
);

export default Router;
