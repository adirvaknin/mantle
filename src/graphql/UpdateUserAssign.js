import gql from "graphql-tag";

const UpdateUserAssign = gql`
  mutation UpdateUserAssign(
    $assignID: Int!
    $groupID: Int!
    $userRoleId: Int!
    $newDetails: jsonb!
  ) {
    update_core_assigns(
      where: { id: { _eq: $assignID } }
      _set: { group_id: $groupID }
    ) {
      affected_rows
    }
    update_core_users_roles(
      _set: { details: $newDetails }
      where: { id: { _eq: $userRoleId } }
    ) {
      affected_rows
    }
  }
`;

export default UpdateUserAssign;
