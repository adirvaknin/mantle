import gql from "graphql-tag";

const GroupChildsByAPI = gql`
  query($groupId: Int!) {
    api_all_groups(where: { id: { _eq: $groupId } }) {
      children {
        id
        name
      }
    }
  }
`;

export default GroupChildsByAPI;
