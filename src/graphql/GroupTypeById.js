import gql from "graphql-tag";

const GroupTypeById = gql`
  query($groupId: Int!) {
    core_groups(where: { id: { _eq: $groupId } }) {
      id
      name
      group_type {
        id
        type
      }
    }
  }
`;

export default GroupTypeById;
