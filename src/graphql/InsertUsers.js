import gql from "graphql-tag";

const InsertUsers = gql`
  mutation($soldiersData: [core_users_insert_input!]!) {
    insert_core_users(
      objects: $soldiersData
      on_conflict: {
        constraint: core_users_soldier_id_unique
        update_columns: soldier_id
      }
    ) {
      affected_rows
    }
  }
`;
export default InsertUsers;
