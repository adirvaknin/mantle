import gql from "graphql-tag";

const RemoveUserAssign = gql`
  mutation RemoveUserAssign($assignID: Int!) {
    delete_core_assigns(where: { id: { _eq: $assignID } }) {
      affected_rows
    }
  }
`;

export default RemoveUserAssign;
