import gql from "graphql-tag";

const AllGroups = gql`
  {
    madors: api_all_madors {
      id
      name
      megamot {
        id
        name
        cycles {
          id
          name
          courses {
            id
            name
            departments {
              id
              name
            }
            teams {
              id
              name
            }
          }
        }
        courses {
          id
          name
          departments {
            id
            name
          }
          teams {
            id
            name
          }
        }
      }
    }
  }
`;

export default AllGroups;
