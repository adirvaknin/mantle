import gql from "graphql-tag";

const EditGroup = gql`
  mutation UpdateGroup($groupId: Int!, $NewGroupName: String!) {
    update_core_groups(
      where: { id: { _eq: $groupId } }
      _set: { name: $NewGroupName }
    ) {
      affected_rows
    }
  }
`;

export default EditGroup;
