import gql from "graphql-tag";

const GroupSiblingsByAPI = gql`
  query($groupId: Int!) {
    api_all_groups(where: { id: { _eq: $groupId } }) {
      parent {
        children {
          id
          name
        }
      }
    }
  }
`;

export default GroupSiblingsByAPI;
