import gql from "graphql-tag";

const DeleteGroup = gql`
  mutation($groupId: Int!) {
    delete_core_groups(where: { id: { _eq: $groupId } }) {
      returning {
        id
      }
    }
  }
`;

export default DeleteGroup;
