import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useMutation } from "@apollo/react-hooks";
import RemoveUserAssign from "../../graphql/RemoveUserAssign";
import UsersAssignedToGroupByAPI from "../../graphql/UsersAssignedToGroupByAPI";
import { Typography, Grid, IconButton } from "@material-ui/core";
import { useParams } from "react-router-dom";
import RemoveIcon from "@material-ui/icons/Remove";
import ForwardIcon from "@material-ui/icons/Forward";

const useStyles = makeStyles(() => ({
  title: {
    fontSize: 14
  },
  Icons: {
    padding: 0
  },
  Direction: {
    direction: "rtl"
  },
  currentGroupAssignBold: {
    fontWeight: "bold",
    textDecoration: "underline"
  }
}));

const GroupLable = ({ group, setModalOpen }) => {
  const classes = useStyles();
  const [removeAssign] = useMutation(RemoveUserAssign);
  const { groupId } = useParams();
  return (
    <Grid container>
      <Grid container item direction="row" spacing={3}>
        <Grid item xs={3}>
          <Typography
            className={`${classes.title} ${classes.currentGroupAssignBold}`}
            color="textSecondary"
            gutterBottom
          >
            {group.group_name}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <IconButton
            className={classes.Icons}
            onClick={() => {
              removeAssign({
                variables: { assignID: group.assigns_id },
                refetchQueries: [
                  {
                    query: UsersAssignedToGroupByAPI,
                    variables: { groupId: groupId }
                  }
                ]
              });
            }}
          >
            <RemoveIcon style={{ color: "red", fontSize: "70%" }} />
          </IconButton>
        </Grid>
        <Grid item xs={3}>
          <IconButton
            className={classes.Icons}
            onClick={() => setModalOpen(true)}
          >
            <ForwardIcon style={{ color: "grey", fontSize: "70%" }} />
          </IconButton>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default GroupLable;
