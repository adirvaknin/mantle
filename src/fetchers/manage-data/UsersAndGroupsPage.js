import React, { useState, useEffect } from "react";
import {
  GridList,
  makeStyles,
  TextField,
  Grid,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  Menu,
  MenuItem,
  Button,
  Tooltip
} from "@material-ui/core";
import { useQuery, useMutation } from "@apollo/react-hooks";
import UserCard from "./UserCard";
import InsertGroup from "../../graphql/InsertGroup";
import AllGroups from "../../graphql/AllGroups";
import GroupSiblingsByAPI from "../../graphql/GroupSiblingsByAPI";
import GroupChildsByAPI from "../../graphql/GroupChildsByAPI";
import UsersAssignedToGroupByAPI from "../../graphql/UsersAssignedToGroupByAPI";
import GroupTypeById from "../../graphql/GroupTypeById";
import DeleteGroup from "../../graphql/DeleteGroup";
import EditGroup from "../../graphql/EditGroup";
import Swal from "sweetalert2";
import * as R from "ramda";
import { useParams, useHistory } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  gridList: {
    width: "100%",
    height: "80vh",
    direction: "rtl",
    marginTop: "1% !important"
  },
  container: {
    marginTop: theme.spacing(4)
  },
  searchAndFilter: {
    direction: "rtl",
    marginTop: "-1%",
    margin: "8"
  },
  Direction: {
    direction: "rtl"
  },
  disabledButton: {
    opacity: 0.5
  }
}));

const UsersAndGroupsPage = () => {
  const [roleFilter, setRoleFilter] = useState([]);
  const [nameAndSoldierIdFilter, setNameAndSoldierIdFilter] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedRoleIndex, setSelectedRoleIndex] = useState(0);
  const classes = useStyles();
  const { groupId } = useParams();
  const history = useHistory();
  useEffect(() => {
    setSelectedRoleIndex(0);
  }, [groupId]);
  const usersAssignedToSelectedGroupQuery = useQuery(
    UsersAssignedToGroupByAPI,
    {
      variables: { groupId: groupId }
    }
  );
  const groupTypeQuery = useQuery(GroupTypeById, {
    variables: { groupId: groupId }
  });
  const GroupSiblingsQuery = useQuery(GroupSiblingsByAPI, {
    variables: { groupId: groupId }
  });
  const GroupChildsQuery = useQuery(GroupChildsByAPI, {
    variables: { groupId: groupId }
  });
  const [insertGroupMutation] = useMutation(InsertGroup);
  const [deleteGroupMutation] = useMutation(DeleteGroup);
  const [editGroupMutation] = useMutation(EditGroup);
  const roleOptions = [
    "הכל",
    "מדריך",
    "חניך",
    'צפ"ה',
    'מק"ס',
    'מ"מ',
    'רמ"ד',
    "מפקד יחידה"
  ];

  const handleClickRoleListItem = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleRoleMenuItemClick = index => {
    setSelectedRoleIndex(index);
    roleOptions[index] === "הכל"
      ? setRoleFilter([])
      : setRoleFilter([
          user =>
            user.assignments.filter(
              assign => assign.group_id === parseInt(groupId)
            )[0]
              ? user.assignments.filter(
                  assign => assign.group_id === parseInt(groupId)
                )[0].role_name === roleOptions[index]
              : []
        ]);
    setAnchorEl(null);
  };

  const handleRoleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleOnChange = event => {
    const searchText = event.target.value;
    searchText === ""
      ? setNameAndSoldierIdFilter([])
      : setNameAndSoldierIdFilter([
          user =>
            isNaN(searchText)
              ? user.first_name.concat(" ", user.last_name).includes(searchText)
              : user.soldier_id.toString().startsWith(searchText)
        ]);
  };

  return usersAssignedToSelectedGroupQuery.loading ||
    groupTypeQuery.loading ||
    GroupSiblingsQuery.loading ||
    GroupChildsQuery.loading ? (
    <div className={classes.container}>
      <Grid container direction="row" justify="center" alignItems="center">
        <CircularProgress />
      </Grid>
    </div>
  ) : (
    <div className={classes.container}>
      <Grid
        container
        direction="row"
        alignItems="center"
        className={classes.searchAndFilter}
      >
        <Grid item>
          <TextField
            label="חיפוש"
            style={{ margin: 8 }}
            placeholder="הקלד שם/מספר אישי"
            type="search"
            onChange={handleOnChange}
          />
        </Grid>
        <Grid item style={{ marginRight: "1%" }}>
          <List>
            <ListItem button onClick={handleClickRoleListItem}>
              <ListItemText
                primary="תפקיד"
                secondary={roleOptions[selectedRoleIndex]}
              />
            </ListItem>
          </List>
          <Menu
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleRoleMenuClose}
            style={{ direction: "rtl" }}
          >
            {roleOptions.map((option, index) => (
              <MenuItem
                key={option}
                selected={index === selectedRoleIndex}
                onClick={() => handleRoleMenuItemClick(index)}
              >
                {option}
              </MenuItem>
            ))}
          </Menu>
        </Grid>
        {![1, 3].includes(groupTypeQuery.data.core_groups[0].group_type.id) && (
          <Grid item style={{ marginRight: "3%" }}>
            <Button
              size="medium"
              variant="outlined"
              onClick={async () => {
                const inputOptions = {
                  2: { 4: "מחזור", 5: "קורס" },
                  4: { 5: "קורס" },
                  5: { 3: "מחלקה", 1: "צוות" },
                  7: { 2: "מגמה" },
                  8: { 7: "מדור" }
                };
                Swal.mixin({
                  input: "text",
                  confirmButtonText: "הבא",
                  cancelButtonText: "ביטול",
                  showCancelButton: true,
                  customClass: classes.Direction,
                  progressSteps: ["1", "2"]
                })
                  .queue([
                    {
                      title: "מה סוג הקבוצה?",
                      input: "radio",
                      inputOptions:
                        inputOptions[
                          groupTypeQuery.data.core_groups[0].group_type.id
                        ],
                      customClass: classes.Direction,
                      inputValidator: value => {
                        return !value ? "חובה לבחור בקבוצה!" : null;
                      }
                    },
                    {
                      title: "מה שם הקבוצה?",
                      input: "text",
                      inputValidator: value => {
                        return !value ? "חייב לתת שם לקבוצה!" : null;
                      }
                    }
                  ])
                  .then(result => {
                    if (result.value) {
                      insertGroupMutation({
                        variables: {
                          groupTypeId: result.value[0],
                          groupName: result.value[1],
                          groupParentId: groupId
                        },
                        refetchQueries: [{ query: AllGroups }]
                      }).then(() => {
                        const Toast = Swal.mixin({
                          toast: true,
                          showConfirmButton: false,
                          timer: 1500
                        });
                        Toast.fire({
                          icon: "success",
                          title: "הקבוצה נוספה"
                        });
                      });
                    }
                  });
              }}
            >
              הוספת תת קבוצה
            </Button>
          </Grid>
        )}
        <Grid item style={{ marginRight: "3%" }}>
          <Button
            size="medium"
            variant="outlined"
            onClick={async () => {
              Swal.fire({
                title: "הקלד שם חדש לקבוצה",
                input: "text",
                confirmButtonText: "הבא",
                cancelButtonText: "ביטול",
                showCancelButton: true,
                customClass: classes.Direction,
                inputValidator: value => {
                  return !value ? "חובה להקליד שם!" : null;
                }
              }).then(result => {
                if (result.value) {
                  editGroupMutation({
                    variables: {
                      groupId: groupId,
                      NewGroupName: result.value
                    },
                    refetchQueries: [{ query: AllGroups }]
                  }).then(() => {
                    const Toast = Swal.mixin({
                      toast: true,
                      showConfirmButton: false,
                      timer: 1500
                    });
                    Toast.fire({
                      icon: "success",
                      title: "הקבוצה עודכנה"
                    });
                  });
                }
              });
            }}
          >
            עריכת שם הקבוצה
          </Button>
        </Grid>
        <Grid item style={{ marginRight: "3%" }}>
          <Tooltip
            className={classes.Direction}
            title={
              usersAssignedToSelectedGroupQuery.data.api_all_users.length
                ? ".לא ניתן למחוק קבוצה זו כיוון שיש חניכים ו/או סגלים השייכים אליה"
                : GroupChildsQuery.data.api_all_groups[0].children.length
                ? ".לא ניתן למחוק קבוצה זו כיוון שיש לה קבוצות בת"
                : ""
            }
          >
            <Button
              color="secondary"
              size="medium"
              variant="outlined"
              disableRipple={
                usersAssignedToSelectedGroupQuery.data.api_all_users.length >
                  0 ||
                GroupChildsQuery.data.api_all_groups[0].children.length > 0
              }
              className={
                usersAssignedToSelectedGroupQuery.data.api_all_users.length ||
                GroupChildsQuery.data.api_all_groups[0].children.length
                  ? classes.disabledButton
                  : null
              }
              onClick={
                usersAssignedToSelectedGroupQuery.data.api_all_users.length ||
                GroupChildsQuery.data.api_all_groups[0].children.length
                  ? null
                  : async () => {
                      Swal.fire({
                        title: "התראת מחיקה",
                        text:
                          "פעולת מחיקת קבוצה כוללת מחיקת כל המידע הקשור לאותה קבוצה כמו ציונים, קשרים וכו'. " +
                          "אם ברצונכם להמשיך למחיקת הקבוצה הקלידו את שם הקבוצה : " +
                          groupTypeQuery.data.core_groups[0].name,
                        icon: "warning",
                        input: "text",
                        showCancelButton: true,
                        focusCancel: true,
                        confirmButtonColor: "#d33",
                        confirmButtonText: "מחק",
                        cancelButtonText: "בטל",
                        customClass: classes.Direction,
                        inputValidator: value => {
                          return !value ? "חובה להקליד את שם הקבוצה!" : null;
                        }
                      }).then(result => {
                        const Toast = Swal.mixin({
                          toast: true,
                          showConfirmButton: false,
                          timer: 1500
                        });
                        result.value === groupTypeQuery.data.core_groups[0].name
                          ? deleteGroupMutation({
                              variables: {
                                groupId: groupId
                              },
                              refetchQueries: [{ query: AllGroups }]
                            }).then(() => {
                              Toast.fire({
                                icon: "success",
                                title: "הקבוצה נמחקה"
                              });
                              history.push("/");
                            })
                          : Toast.fire({
                              icon: "error",
                              title: "הפעולה בוטלה"
                            });
                      });
                    }
              }
            >
              מחיקת קבוצה
            </Button>
          </Tooltip>
        </Grid>
      </Grid>
      <GridList className={classes.gridList}>
        {usersAssignedToSelectedGroupQuery.data ? (
          R.filter(R.allPass(roleFilter.concat(nameAndSoldierIdFilter)))(
            usersAssignedToSelectedGroupQuery.data.api_all_users
          ).map(user => (
            <UserCard
              key={user.soldier_id}
              user={user}
              groupSiblings={
                GroupSiblingsQuery.data.api_all_groups[0].parent.children
              }
            />
          ))
        ) : (
          <div></div>
        )}
      </GridList>
    </div>
  );
};
export default UsersAndGroupsPage;
