import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { runWithAdal } from "react-adal";
import { authContext } from "./react-adal.js";
import * as serviceWorker from "./serviceWorker";

runWithAdal(
  authContext,
  () => {
    ReactDOM.render(<App />, document.getElementById("root"));
  },
  false
);

serviceWorker.unregister();
