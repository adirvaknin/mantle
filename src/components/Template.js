import React from "react";
import NavBar from "./NavBar";
import GroupSideBar from "./GroupSideBar";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  scrollbar: {
    overflowY: "auto",
    height: "98vh",
    direction: "ltr"
  }
}));

export default ({ children }) => {
  const classes = useStyles();
  return (
    <Grid container direction="row" style={{ direction: "rtl" }}>
      <Grid item md={2} className={classes.scrollbar}>
        <GroupSideBar />
      </Grid>
      <Grid container item direction="column" md={10}>
        <NavBar />
        <main>{children}</main>
      </Grid>
    </Grid>
  );
};
