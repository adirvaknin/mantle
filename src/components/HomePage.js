import React from "react";
import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles(() => ({
  headerIMG: {
    width: "70px",
    height: "70px",
    marginBottom: "-8%"
  },
  bodyText: {
    marginTop: "3%"
  }
}));
const HomePage = () => {
  const classes = useStyles();
  return (
    <div>
      <h1>
        <img className={classes.headerIMG} src="logo.png" alt="מנטל" />| ברוכים
        הבאים למנטל !
      </h1>
      <h2 className={classes.bodyText}>נא לבחור קבוצה כדי להמשיך.</h2>
    </div>
  );
};

export default HomePage;
